import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, IconButton, Typography, Button, Container } from '@material-ui/core';

import MenuIcon from '@material-ui/icons/Menu';
import Home from '../views/Home'
import { grey } from '@material-ui/core/colors';
import logo from '../assets/logo.png';
import logoPlus from '../assets/logo_plus.png';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        height: '100vh',
        background: grey[100]
    },
    grow: {
        flexGrow: '1',
    },
    appBar: {
        borderBottom: '1px solid lightgrey'
    },
    title: {
        color: theme.palette.secondary.main,
    },
    logo: {
        height: "30px",
    }
}));


function MainLayout(params) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static" elevation={0} className={classes.appBar}>
                <Container>
                    <Toolbar>
                        {/* <IconButton edge="start" className={classes.menuButton} color="secondary" aria-label="menu">
                            <MenuIcon />
                        </IconButton> */}
                        <img src={logoPlus} className={classes.logo}></img>
                        <div className={classes.grow} />

                        <Typography variant="overline" color="secondary" className={classes.title}>
                            Sobre
                        </Typography>
                        {/* <div className={classes.grow} /> */}
                        {/* <Button color="inherit" color="secondary">Login</Button> */}
                    </Toolbar>
                </Container>
            </AppBar>
            <Home />
        </div>
    )
}

export default MainLayout;