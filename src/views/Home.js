import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import { Container, Typography, Grid, Box } from '@material-ui/core'
import logo from '../assets/andre_orlandi.jpg'

const useStyles = makeStyles((theme) => ({
    root: {

    },
    large: {
        width: theme.spacing(57),
        height: theme.spacing(57),
    },
    box: {
        margin: theme.spacing(3)
    },
    hello: {
        fontWeight: "bold",
        marginTop: theme.spacing(5),
        marginBottom: theme.spacing(0),
        color: theme.palette.secondary.main,
    },
    title: {
        fontWeight: "bold",
        marginTop: theme.spacing(0),
        marginBottom: theme.spacing(3),
        color: "gold",
    },
    subtitle: {
        // fontWeight: "bold",
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(5),
        color: theme.palette.secondary.main,
    },
    container: {
        marginTop: theme.spacing(5),
        marginBottom: theme.spacing(5),
    }
}));

function Home() {
    const classes = useStyles();

    return (
        <Container>
            <Grid container alignItems="flex-start" className={classes.container}>
                <Grid item xs={12} md={6}>
                    <Box display="flex" flexDirection="column" flexGrow={1} className={classes.box} p={5} spacing={0}>
                        <Typography variant="h2" className={classes.hello}>Olá, meu nome é</Typography>
                        <Typography variant="h2" className={classes.title}>  André Orlandi </Typography>
                        <Typography variant="h4" className={classes.subtitle}>Estou preparando muita coisa legal para colocar aqui para vocês.</Typography>
                    </Box>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Box display="flex" flexGrow={1} justifyContent="center" className={classes.box} alignItems="center">
                        <Avatar src={logo} className={classes.large}></Avatar>
                    </Box>
                </Grid>
                <Grid item style={{ height: "100%" }}>
                </Grid>
            </Grid>
        </Container>
    )
}

export default Home;