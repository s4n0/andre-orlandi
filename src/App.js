import React from 'react'
import { makeStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { AppBar, Toolbar, IconButton, Typography, Button, Container, CssBaseline } from '@material-ui/core';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import MenuIcon from '@material-ui/icons/Menu';

import MainLayout from './layouts/MainLayout'
import Home from './views/Home'


function App() {
  // const classes = useStyles();

  const theme = createMuiTheme({
    palette: {
      type: 'light',
      primary: {
        main: '#fff'//#003355'
      },
      secondary: {
        main: '#666655'
      },
    },
    typography: {
      fontFamily: ['Roboto']
    }
  });

  return (
    <React.Fragment>
      <CssBaseline />
      <ThemeProvider theme={theme}>
        <Router>
          <MainLayout>
            <Switch>
              <Route exact path="/" component={Home} />
              {/* <Route exact path="/about" component={About} />
              <Route exact path="/contact" component={Contact} />
              <Route exact path="/patients" component={Patients} /> */}
              {/* <Route component={Error404} /> */}
            </Switch>
          </MainLayout>
        </Router>
      </ThemeProvider>
    </React.Fragment>
  );
}

export default App;
